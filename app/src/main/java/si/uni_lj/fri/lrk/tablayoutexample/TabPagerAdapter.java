package si.uni_lj.fri.lrk.tablayoutexample;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class TabPagerAdapter extends FragmentStateAdapter {
    private int tabCount;
    @NonNull
    @Override
    public Fragment createFragment(int position) {

        if(position==0){
            return new Tab1Fragment();
        }
        else if(position==1){
            return new Tab2Fragment();
        }
        else{
            return new Tab3Fragment();
        }
    }

    @Override
    public int getItemCount() {
        return tabCount;
    }
    public TabPagerAdapter(FragmentActivity fa, int numOfTabs){
        super(fa);
        tabCount=numOfTabs;
    }
}
